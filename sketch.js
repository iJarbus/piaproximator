const radius = 400,
	  lineWidth = 1,
	  numLoopsPerDraw = 1000;
	  
let totalPointsGenerated = 0,
	totalPointsInCircle = 0,
	livingPoints = [],
	pi = 0;
	x = 0;
	y = 0;


function setup() {
	createCanvas(((radius + 1) * 2), ((radius + 1) * 2));
	background(0);
	translate(width / 2, height / 2);
	stroke(0, 255, 0);
	strokeWeight(lineWidth);
	noFill();
	ellipse(0, 0, radius * 2, radius * 2);
	rectMode(CENTER);
	rect(0, 0, radius * 2, radius * 2);
	
}

function draw() {
	// This is done every frame instead of every point generate since the translate function is quite expensive so we want to minimize calls to it.
	translate(width / 2, height / 2);
	for(i = 0; i <= numLoopsPerDraw; i++) {
		let newPoint = new drawnPoint();
	}

	calculatePi();
	printPi();
}

function calculatePi() {
	pi = (totalPointsInCircle * 4) / totalPointsGenerated;
}

function printPi() {
	document.getElementById("piText").innerHTML = pi;
}

class drawnPoint {
	constructor() {
		totalPointsGenerated++;
		this.x = random(-radius, radius);
		this.y = random(-radius, radius);
		
		this.maxLifeTime = 1000;
		this.currentLifetime = 0;
		
		if(this.x * this.x + this.y * this.y <= radius * radius) {
			this.isInCircle = true;
			stroke(255, 0, 0);
			totalPointsInCircle++;
		}
		else {
			stroke(0, 0, 255);
		}
		
		point(this.x, this.y);
	}

	CalcIsInCircle() {
		translate(width / 2, height / 2);
		return x*x + y*y < radius * radius;
	}

	update() {
	}
}